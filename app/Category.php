<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    public function products(){
        return $this->belongsToMany('App\Product', 'products_categories', 'category_id', 'product_id');
    }
}
