<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use App\Product;


class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
        // get all the products
        $products = Product::all();
        $data = [
            'products' => $products,
        ];

        return view('welcome')->with($data);
    }

}