<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the products
        $products = Product::all();

        // load the view and pass the products
        return \View::make('products.index')
            ->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $allCategories = Category::pluck('title', 'id')->all();
        $data = [
            'allCategories' => $allCategories
        ];
        return view('products.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        $rules = array(
            'name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/products/create')
                ->withErrors($validator);
        } else {

            try {
                DB::beginTransaction();
                // store
                $product = new Product;
                $product->name = Input::get('name');
                $product->description = Input::get('description');
                $product->code = Input::get('code');
                $product->category_id = Input::get('category_id');
                $product->price = Input::get('price');
                $result = $product->save();

                if (Input::file('image') && $result) {
                    $image = Input::file('image');
                    $fileName = time() . '-' . md5($image->getClientOriginalName());
                    $input['imagename'] = $fileName . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('/images/products/' . $product->id);
                    $image->move($destinationPath, $input['imagename']);
                    $product->filename = $input['imagename'];
                    $product->filepath = '/images/products/' . $product->id . "/" . $input['imagename'];
                    $product->save();
                }

                DB::commit();
                // redirect
                Session::flash('message', 'Successfully created product!');
                return Redirect::to('admin/products');
            } catch (\Exception $e) {
                DB::rollback();
                return Redirect::to('admin/products/create')
                    ->withErrors($validator);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $data = [
            'product' => $product,
        ];
        return view('products.view')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $allCategories = Category::pluck('title', 'id')->all();
        $product = Product::find($id);
        $data = [
            'allCategories'      => $allCategories,
            'product'            => $product,
        ];
        return view('products.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $product = Product::find($id);
        // validate
        $rules = array(
            'name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/products/edit/' . $id)
                ->withErrors($validator);
        } else {

            try {
                DB::beginTransaction();
                // store
                $product->name = Input::get('name');
                $product->description = Input::get('description');
                $product->code = Input::get('code');
                $product->category_id = Input::get('category_id');
                $product->price = Input::get('price');
                $result = $product->save();

                if (Input::file('image') && $result) {
                    $image = Input::file('image');
                    $fileName = time() . '-' . md5($image->getClientOriginalName());
                    $input['imagename'] = $fileName . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('/images/products/' . $product->id);
                    $image->move($destinationPath, $input['imagename']);
                    $product->filename = $input['imagename'];
                    $product->filepath = '/images/products/' . $product->id . "/" . $input['imagename'];
                    $product->save();
                }

                DB::commit();
                // redirect
                Session::flash('message', 'Successfully edit product!');
                return Redirect::to('admin/products');
            } catch (\Exception $e) {
                DB::rollback();
                Session::flash('error', 'Error edit product!');
                return Redirect::to('admin/products/edit/' . $id)
                    ->withErrors($validator);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $product = Product::find($id);
        $product->delete();

        //To Do
        //Remove image folder

        // redirect
        Session::flash('message', 'Successfully deleted the product!');
        return Redirect::to('admin/products');
    }
}
