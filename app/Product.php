<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'code'
    ];


    public function category(){
        return $this->belongsTo('App\Category', 'category_id');
    }

}
