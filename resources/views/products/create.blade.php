@extends('adminlte::page')

@section('title', 'Product Manager')

@section('content_header')
    <h1>Product Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')
    {{  Form::open(array('url'=>'admin/products/store', 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" placeholder="Name">
        @if ( $errors->has('name') )
            <span class="text-danger">{{{ $errors->first('name') }}}</span>
        @endif
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea type="text" name="description" class="form-control" placeholder="Description"></textarea>
    </div>

    <div class="form-group">
        <label for="title">Category</label>
        {!! Form::select('category_id',$allCategories, old('category_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
        @if ( $errors->has('category_id') )
            <span class="text-danger">{{{ $errors->first('category_id') }}}</span>
        @endif
    </div>


    <div class="form-group">
        <label for="code">Code</label>
        <input type="text" name="code" autocomplete="off" class="form-control" placeholder="Code">
        @if ( $errors->has('code') )
            <span class="text-danger">{{{ $errors->first('code') }}}</span>
        @endif
    </div>

    <div class="form-group">
        <label for="price">Price</label>
        <input type="text" name="price" autocomplete="off" class="form-control" placeholder="Price">
        @if ( $errors->has('price') )
            <span class="text-danger">{{{ $errors->first('price') }}}</span>
        @endif
    </div>

    <div class="form-group">
        <label for="image">Image</label>
        <input type="file" class="form-control" name="image" placeholder="Upload Image" multiple="false">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    {{ Form::close() }}

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop