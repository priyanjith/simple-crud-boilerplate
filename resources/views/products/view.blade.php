@extends('adminlte::page')

@section('title', 'Product Manager')

@section('content_header')
    <h1>Product Manager</h1>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@stop

@section('content')

    <div class="form-group">
        <label for="name">Name</label>
        <p for="title">{{ isset($product) ? $product->name : "-" }}</p>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <p for="title">{{ isset($product) ? $product->description : "-" }}</p>
    </div>

    <div class="form-group">
        <label for="categories">Category</label>
        <p for="title">{{ isset($product->category->title) ? $product->category->title : "-" }}</p>
    </div>

    <div class="form-group">
        <label for="code">Code</label>
        <p for="title">{{ isset($product) ? $product->code : "-" }}</p>
    </div>

    <div class="form-group">
        <label for="price">Price</label>
        <p for="title">{{ isset($product) ? $product->price : "-" }}</p>
    </div>

    <div class="form-group">
        <label for="image">Image</label>
        @if($product->filepath)
            <div>
                <div class="img-wrap">
                    <span class="close">&times;</span>
                    <img src="{{asset($product->filepath)}}" data-id="{{ $product->id }}" with="75px" height="75px">
                </div>
            </div>
        @endif
    </div>

    <a href="/admin/products">
        <button type="button" class="btn btn-info">Back</button>
    </a>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('js')
@stop